import model.Person;
import model.Staff;
import model.Student;

public class App {
    public static void main(String[] args) throws Exception {


        Person person1 = new Person("cuong", "sai gon");
        Person person2 = new Person("phuong", "tien giang");

        System.out.println(person1.toString());
        System.out.println(person2.toString());


        Student student1 = new Student("cuong", "sai gon", "java", 2023, 0);
        Student student2 = new Student("phuong", "tien giang", "java", 2023, 0);

        System.out.println(student1.toString());
        System.out.println(student2.toString());

        Staff staff1 = new Staff("cuong", "sai gon", "iuh", 5000);
        Staff staff2 = new Staff("phuong", "tien giang", "iuh", 6000);

        System.out.println(staff1.toString());
        System.out.println(staff2.toString());

    }
}
